//
//  AssistView.swift
//  Leocat
//
//  Created by Korakod Saraboon on 12/2/2567 BE.
//

import SwiftUI

struct AssistView: View {
  @EnvironmentObject private var globalObject: GlobalObject
  @FocusState private var focusedField: Bool
  @State private var inputText: String = ""
  @State private var outputText: String = ""
  @State private var isReadyForInput: Bool = true
  @State private var dialogueCount: Int = 0
  @State var dialogueList: [DialogueModel] = []
  
  //MARK: - view variable
  @State private var textBoxBackgroundOpacity: CGFloat = .zero
  @State private var scrollviewOpacity: CGFloat = .zero
  @State private var textBoxTopPadding: CGFloat = .zero
  @State private var textBoxPadding: CGFloat = 200
  @State private var textboxOffsetY: CGFloat = -40
  @State private var aiImageAnimate: Bool = false
  @State private var aiImageOpacity: CGFloat = 0.3
  @State private var aiImageSize: CGFloat = 250
  @State private var isTransition: Bool = false
  @State private var isCompactmode: Bool = true
  @State private var profileOpaqe: CGFloat = .zero
  @State private var offsetX: CGFloat = 80
  @State private var offsetY: CGFloat = 5
  
  @State private var viewModel: AIViewModel?
  
  var body: some View {
    GeometryReader{ geometry in
      VStack{
        HStack(alignment: .top) {
          VStack  {
            ZStack {
              Image("coolM")
                .resizable()
                .opacity(aiImageOpacity)
                .offset(x: offsetX, y: offsetY)
                .aspectRatio(contentMode: .fill)
                .frame(width: aiImageSize, height: aiImageSize, alignment: .center)
                .scaleEffect(aiImageAnimate ? 0.9 : 1)
                .animation(.bouncy(duration: 0.2, extraBounce: 0.5), value: aiImageAnimate)
                .onTapGesture(count: 2, perform: {
                  triggerMode()
                })
                .onLongPressGesture(minimumDuration: .infinity) {
                  aiImageAnimate.toggle()
                } onPressingChanged: { starting in
                  aiImageAnimate.toggle()
                }
            }.frame(width: 1, height: 1)
            Spacer()
          }
          VStack(spacing: .zero) {
            AIProfileView()
              .opacity(profileOpaqe)
              .padding([.top], -15)
            ScrollViewReader { proxy in
              ScrollView {
                AIView(text: outputText).modifier(FlipEffect()).frame(minWidth: 1 ,maxWidth: .infinity, minHeight: 1 , maxHeight: .infinity, alignment: .leading).id("responseview")
                
                ForEach($dialogueList.reversed(), id: \.id) { dialogue in
                  DialogueView(dialogue: dialogue)
                    .listRowBackground(Color.clear)
                    .listRowSeparator(.hidden)
                    .modifier(FlipEffect())
                    .padding([.horizontal], 5)
                }
              }.frame(minWidth: 1,
                      idealWidth: 400,
                      maxWidth: .infinity,
                      minHeight: 1,
                      idealHeight: 600,
                      maxHeight: .infinity)
              .scrollContentBackground(.hidden)
              .opacity(scrollviewOpacity)
              .modifier(FlipEffect())
              .onChange(of: dialogueList.count) {
                if dialogueList.last?.type == .user {
                  proxy.scrollTo("responseview", anchor: .bottom)
                }
              }
            }
            ZStack(alignment: .bottomTrailing) {
              TextEditor(text: $inputText)
                .opacity(0.8)
                .padding([.vertical], textBoxTopPadding)
                .font(.custom("Noboto-Regular", size: 20))
                .scrollContentBackground(.hidden)
                .background(.opacity(textBoxBackgroundOpacity))
                .frame(minWidth: 50,
                       idealWidth: 640,
                       maxWidth: .infinity,
                       minHeight: 10,
                       maxHeight: isCompactmode ? 40 : geometry.size.height / 2.5)
                .fixedSize(horizontal: false, vertical: true)
                .onKeyPress { press in
                  if (press.key == KeyEquivalent.return && !press.modifiers.contains(EventModifiers.shift)) {
                    generateResponse()
                    return .handled
                  }
                  if press.key == KeyEquivalent.tab {
                    
                    return .handled
                  }
                  return .ignored
                }
              Button {
                generateResponse()
              } label: {
                Image(systemName: "paperplane.fill")
                  .resizable()
                  .foregroundColor(.accent)
                  .opacity(isCompactmode ? .zero : 0.5)
                  .frame(width: isCompactmode ? 1 : 20, height: 20)
              }.buttonStyle(.plain)
                .padding([.bottom, .trailing], 10)
            }
            .padding([.leading], textBoxPadding)
            .clipShape(RoundedRectangle(cornerRadius: 9))
            .onChange(of: geometry.size.height) {
              setAiImageSizeWithAnim(height: geometry.size.height)
              setTextBoxSizeWithAnim(height: geometry.size.height)
              setTextBoxBackgroundOpacity(height: geometry.size.height)
            }
            .offset(x: .zero, y: textboxOffsetY)
          }
          .padding([.bottom], 10)
          .padding([.trailing], 10)
          .padding([.leading], 1)
        }
      }.frame(minHeight: 75)
        .task {
          self.viewModel = AIViewModel()
        }
    }
  }
  
  // MARK: - View Controller Logic
  
  private func setTextBoxSizeWithAnim(height: CGFloat) {
    withAnimation(.smooth(duration: 0.3,extraBounce: 0.3)) {
      textBoxPadding = height >= 200 ? .zero :  200
      textBoxTopPadding = height >= 200 ? 10 : .zero
      textboxOffsetY = height >= 200 ? .zero : -40
      
      if !isTransition {
        isCompactmode = !(height >= 200)
        if isCompactmode {
          globalObject.currentState = .compact
        }
      }
    }
  }
  
  private func setTextBoxBackgroundOpacity(height: CGFloat) {
    withAnimation(.easeInOut(duration: 0.3)) {
      textBoxBackgroundOpacity = height <= 200 ? .zero : 0.1
      scrollviewOpacity = height <= 200 ? .zero : 1
    }
  }
  
  private func setAiImageSizeWithAnim(height: CGFloat) {
    withAnimation(.bouncy(extraBounce: 0.25)) {
      aiImageSize = height <= 200 ? 250 : 50
      aiImageOpacity = height <= 200 ? 0.3 : .zero
      offsetX = height <= 150 ? 80 : 40
      offsetY = height <= 150 ? 5 : 10
      profileOpaqe = height <= 200 ? .zero : 1
    }
  }
  
  private func triggerMode() {
    isTransition = true
    globalObject.setAssistMode(mode: isCompactmode ? .window : .compact)
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
      isTransition = false
      isCompactmode = !isCompactmode
    }
  }
  
  private func addDialogue(text: String, type: DialogueType) {
    if !outputText.isEmpty {
      let temp = outputText
      outputText = ""
      dialogueCount += 1
      dialogueList.append(DialogueModel(id: dialogueCount, type: .ai, textOutput: temp))
    }
    dialogueCount += 1
    dialogueList.append(DialogueModel(id: dialogueCount, type: .user, textOutput: text))
  }
  
  private func generateResponse() {
    guard !inputText.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty, isReadyForInput else { return }
    
    if isCompactmode {
      triggerMode()
    }
    
    let tempInput = inputText.trimmingCharacters(in: .whitespacesAndNewlines)
    addDialogue(text: tempInput, type: .user)
    inputText = ""
    
    Task {
      viewModel?.preProcess = { input, history in
        let systemPrompt = "Your name is LEOC an conding AI Assistant in Leo squad. You are a helpful, respectful and honest assistant with a deep knowledge of code and software design. Always answer as helpfully as possible, while being safe. Your answers should not include any harmful, unethical, racist, sexist, toxic, dangerous, or illegal content. Please ensure that your responses are positive in nature. If a question does not make any sense, or is not factually coherent, explain why instead of answering something not correct."
        
        var processed = "\(systemPrompt) \n"
        for chat in history {
          if chat.role == .user {
            processed += "### Instruction: \n \(chat.content) \n"
          } else {
            processed += "### Response: \n \(chat.content) \n <|EOT|>"
          }
        }
        processed += "### Instruction: \n \(input) \n ### Response:"
        return processed
      }
      
      viewModel?.postProcess = { output in
        isReadyForInput = true
      }
      
      viewModel?.update = { output in
        outputText += output ?? ""
      }
      
      isReadyForInput = false
      await viewModel?.respond(to: tempInput)
    }
    viewModel?.stop()
  }
}

private func presetCheck(input: String) -> String {
  
  
   return input
}

#Preview {
  AssistView()
    .environmentObject(GlobalObject.shared)
}

struct FlipEffect: GeometryEffect {
  func effectValue(size: CGSize) -> ProjectionTransform {
    let t = CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: size.height)
    return ProjectionTransform(t)
  }
}
