//
//  LeocatApp.swift.swift
//  Leocat
//
//  Created by Korakod Saraboon on 8/2/2567 BE.
//

import SwiftUI

@main
struct LeocatApp: App {
  // MARK: - App state variable
  @State private var globalObject: GlobalObject = GlobalObject.shared
  @Environment(\.scenePhase) private var scenePhase
  
  var body: some Scene {
    // MARK: - Main window
    WindowGroup {
      AssistView()
        .frame(minWidth: globalObject.defaultAppWindowSize.width, minHeight: globalObject.defaultAppWindowSize.height)
        .environmentObject(globalObject)
        .onAppear { initialSetup() }
    }
    .windowResizability(.contentSize)
    .defaultSize(globalObject.defaultAppWindowSize)
    
    // MARK: - Menu bar
    MenuBarExtra {
      Text(globalObject.appInfo)
      Button(scenePhase == .active ? globalObject.menubarHideWindow : globalObject.menubarShowWindow) {
        globalObject.shouldShowWindow(show: scenePhase != .active )
      }
      Divider()
      Button(globalObject.menubarQuit) {
        globalObject.setDefaultWindowPosition()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
          NSApplication.shared.terminate(nil)
        }
      }
    } label: {
      Image(globalObject.menubarImageIcon)
    }
    .menuBarExtraStyle(.menu)
  }
  
  // MARK: - private app function  
  private func initialSetup() {
    globalObject.setupTitleBar()
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
      globalObject.setDefaultWindowPosition()
    }
  }
}

struct VisualEffect: NSViewRepresentable {
  func makeNSView(context: Self.Context) -> NSView { return NSVisualEffectView() }
  func updateNSView(_ nsView: NSView, context: Context) { }
}
