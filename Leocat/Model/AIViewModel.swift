//
//  AIViewModel.swift
//  Leocat
//
//  Created by Korakod Saraboon on 13/2/2567 BE.
//

import Foundation
import SwiftUI
import LLM

@Observable
class AIViewModel: LLM {
  convenience init() {
    let url = Bundle.main.url(forResource: "deepseek-coder-6.7b-instruct.Q5_K_M", withExtension: "gguf")!
    self.init(from: url, stopSequence: "###", topK: 1, topP: 0.75, temp: 0.1, historyLimit: 3, maxTokenCount: 4096)
  }
}
