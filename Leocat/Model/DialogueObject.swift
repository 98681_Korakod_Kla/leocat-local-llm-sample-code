//
//  DialogueObject.swift
//  Leocat
//
//  Created by Korakod Saraboon on 22/2/2567 BE.
//

import Foundation
import SwiftUI

public enum DialogueType {
  case user
  case ai
}

struct DialogueModel: Identifiable {
  var id: Int = 0
  var type: DialogueType
  var textOutput: String
}
