//
//  GlobalObject.swift
//  Leocat
//
//  Created by Korakod Saraboon on 13/2/2567 BE.
//

import SwiftUI
import HotKey

public enum presetMode {
  case discuss
  case codeReview
  case generalCheck
  case mockData
  case unitTest
}

public enum AssistMode {
  case expand
  case compact
  case window
  case fullscreen
}

class GlobalObject: ObservableObject {
  static let shared = GlobalObject()
  
  var defaultAppWindowSize: CGSize = CGSize(width: 640, height: 50)
  var appwindowName: String = "LLAA"
  var appwindowId: String = "llaamain"
  var appInfo: String = "Leo squad offline code AI assistant v.\(Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "")"
  var aiModelInfo: String = "Model : Deepseek Coder 6.7 Instruct"
  var menubarQuit: String = "Quit"
  var menubarShowWindow: String = "Show window"
  var menubarHideWindow: String = "Hide window"
  var menubarImageIcon: String = "cool"
  var currentState: AssistMode = .compact
  var windowStateArray: [AssistMode] = [.compact,.window,.expand]
  var windowIndex: Int = 0
  var currentPresetMode: presetMode = .discuss
  var presetModeArray: [presetMode] = [.discuss,.codeReview,.generalCheck,.mockData,.unitTest]
  var presetIndex: Int = 0
  
  // MARK: - Hotkey
  var openWindowHotkey: HotKey?
  var windowModeHotkey: HotKey?
  var compactModeHotkey: HotKey?
  var expandModeHotkey: HotKey?
  var fullscreenModeHotkey: HotKey?
  
  init() {
    openWindowHotkey = HotKey(key: Key(string: "/")!, modifiers: [.option], keyDownHandler: {
      self.shouldShowWindow(show: !self.isAsistWindowVisible())
    })

    fullscreenModeHotkey = HotKey(key: .leftArrow, modifiers: [ .option], keyDownHandler: {
      self.shouldShowWindow(show: true)
      self.setAssistMode(mode: .fullscreen)
    })
    
    compactModeHotkey = HotKey(key: .rightArrow, modifiers: [ .option], keyDownHandler: {
      self.shouldShowWindow(show: true)
      self.windowIndex = 0
      self.setAssistMode(mode: .compact)
    })
  
    windowModeHotkey = HotKey(key: .upArrow, modifiers: [ .option], keyDownHandler: {
      self.shouldShowWindow(show: true)
      self.windowIndex = max(0, self.windowIndex - 1)
      self.setAssistMode(mode: self.windowStateArray[self.windowIndex])
    })
    
    expandModeHotkey = HotKey(key: .downArrow, modifiers: [ .option], keyDownHandler: {
      self.shouldShowWindow(show: true)
      self.windowIndex = min(2, self.windowIndex + 1)
      self.setAssistMode(mode: self.windowStateArray[self.windowIndex])
    })
  }
  
  // MARK: - Global function
  func setDefaultWindowPosition(isAnimate: Bool = false) {
    guard let screen = NSScreen.main else {
      return
    }
    NSApp.windows.first?.setFrame(NSRect(x: screen.frame.width - (screen.frame.width/2) + (screen.frame.width/2) - 640,
                                         y: screen.frame.size.height - 150,
                                         width:  defaultAppWindowSize.width,
                                         height:  defaultAppWindowSize.height),
                                  display: true, animate: isAnimate)
  }
  
  func setAssistMode(mode: AssistMode) {
    guard let screen = NSScreen.main else {
      return
    }
    currentState = mode
    var newHeight: CGFloat = defaultAppWindowSize.height
    var newWidth: CGFloat = defaultAppWindowSize.width
    var posX: CGFloat = screen.frame.width - (screen.frame.width/2) + (screen.frame.width/2) - defaultAppWindowSize.width
    var posY: CGFloat = screen.frame.size.height - 150
    switch mode {
    case .window:
      newHeight = (defaultAppWindowSize.width - (defaultAppWindowSize.width / 3))
    case .fullscreen:
      newHeight = screen.frame.size.height
      newWidth = screen.frame.size.width
      posX = .zero
      posY = .zero
    case .expand:
      newHeight = screen.frame.size.height
    default:
      newWidth = defaultAppWindowSize.width
    }
   
    NSApp.windows.first?.setFrame(
      NSRect(x: posX,
             y: posY,
             width:  newWidth,
             height:  newHeight),
      display: true, animate: true)
      NSApp.activate(ignoringOtherApps: true)
      NSApp.windows.first?.makeKeyAndOrderFront(nil)
  }
  
  func calculatePercentage(value:Double,percentageVal:Double)->Double{
    let val = value * percentageVal
    return val / 100.0
  }
  
  func shouldShowWindow(show: Bool) {
    guard let mainWindow = NSApp.windows.first else {
      return
    }
    if show {
      NSApp.activate(ignoringOtherApps: true)
      mainWindow.makeKeyAndOrderFront(nil)
    } else {
      mainWindow.orderOut(nil)
    }
  }
  
  func isAsistWindowVisible() -> Bool{
    guard let mainWindow = NSApp.windows.first else {
      return false
    }
    return mainWindow.isVisible && mainWindow.isMainWindow
  }
  
  func setupTitleBar() {
    guard let mainWindow = NSApp.windows.first else {
      return
    }
    mainWindow.standardWindowButton(.closeButton)?.isHidden = true
    mainWindow.standardWindowButton(.miniaturizeButton)?.isHidden = true
    mainWindow.standardWindowButton(.zoomButton)?.isHidden = true
    mainWindow.titleVisibility = .hidden
    mainWindow.titlebarAppearsTransparent = true
    mainWindow.isMovableByWindowBackground = true
  }
  
  func changePresetMode() {
    
  }
}
