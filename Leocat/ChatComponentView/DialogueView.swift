//
//  DialogueView.swift
//  Leocat
//
//  Created by Korakod Saraboon on 16/2/2567 BE.
//

import SwiftUI

struct DialogueView: View {
  @Binding var dialogue: DialogueModel
  @ViewBuilder
  var body: some View {
    if dialogue.type == .ai {
      AIView(text: dialogue.textOutput).frame(minWidth: 1 ,maxWidth: .infinity,alignment: .leading)
    } else {
      UserView(text: dialogue.textOutput).frame(minWidth: 1,maxWidth: .infinity)
    }
  }
}

#Preview {
  DialogueView(dialogue: .constant(DialogueModel(id: 0, type: .ai, textOutput: "Test")))
}
