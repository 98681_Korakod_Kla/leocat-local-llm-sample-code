//
//  AIProfileView.swift
//  Leocat
//
//  Created by Korakod Saraboon on 21/2/2567 BE.
//

import SwiftUI

struct AIProfileView: View {
    var body: some View {
      HStack {
        Image("coolM")
          .resizable()
          .frame(width: 50 , height: 50)
        VStack(alignment: .leading) {
          HStack {
            Text("LEOC")
              .font(.custom("Noboto-Regular", size: 18))
              .bold()
              .foregroundStyle(.accent)
            Spacer()
          }
          Text("Leo squad ofline code AI assistant")
            .foregroundStyle(.opacity(0.8))
            .font(.custom("Noboto-Regular", size: 14))
        }
        Spacer()
      }.padding([.bottom], 5)
    }
}

#Preview {
    AIProfileView()
}
