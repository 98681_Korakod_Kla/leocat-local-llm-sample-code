//
//  AIView.swift
//  Leocat
//
//  Created by Korakod Saraboon on 29/2/2567 BE.
//

import SwiftUI
import MarkdownView

struct AIView: View {
  var text: String
  
    var body: some View {
      MarkdownView(text: text)
        .codeHighlighterTheme(.init(lightModeThemeName: "xcode", darkModeThemeName: "androidstudio"))
        .font(.custom("Noboto-Regular", size: 16), for: .body)
        .lineSpacing(8)
        .padding([.vertical], 16)
        .padding([.horizontal], 10)
    }
}

#Preview {
  AIView(text: "Tedt")
}
