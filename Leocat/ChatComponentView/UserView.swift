//
//  UserView.swift
//  Leocat
//
//  Created by Korakod Saraboon on 29/2/2567 BE.
//

import SwiftUI
import MarkdownView

struct UserView: View {
  var text: String
  
    var body: some View {
      HStack {
        Spacer(minLength: 100)
        Text(text)
          .font(.custom("Noboto-Regular", size: 16))
          .padding([.vertical,.horizontal], 10)
          .background(.purple.opacity(0.3))
          .clipShape(.rect(topLeadingRadius: 10,
                          bottomLeadingRadius: 10,
                           bottomTrailingRadius: 0,
                           topTrailingRadius: 10))
      }
    }
}

#Preview {
  UserView(text: "TEXT")
}
